#include <cuda.h>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>

long clock(struct timeval t, struct timeval s){
    return ((s.tv_sec * 1e6 +s.tv_usec ) -  (t.tv_sec * 1e6 +t.tv_usec ) );
}

int main(){
    cudaFree(0);

    float * array = (float *) malloc(sizeof(float)*(int)1e3<<19);
    float * device_array;
    cudaMalloc((void ** ) &device_array, sizeof(float)*(int)1e3<<19);
    struct timeval start_h2d,end_h2d, start_d2h, end_d2h;

    for (int i=0; i<(int)1e3<<19; i++ )
        array[i] = rand()%1000;
    
    for( int i =1e3; i <= (int)1e3<<19; i<<=1 ){
        for( int j=0; j<10; j++){
        gettimeofday(&start_h2d, NULL);
        cudaMemcpy(device_array, array, sizeof(float)*i, cudaMemcpyHostToDevice);
        gettimeofday(&end_h2d, NULL);

        gettimeofday(&start_d2h, NULL);
        cudaMemcpy(array, device_array, sizeof(float)*i, cudaMemcpyDeviceToHost);
        gettimeofday(&end_d2h, NULL);

        printf("%ld,", i );
        printf("%ld,", clock(start_h2d, end_h2d));
        printf("%ld", clock(start_d2h, end_d2h));
        printf("\n");

        }
    }
    


    return 1;

}
