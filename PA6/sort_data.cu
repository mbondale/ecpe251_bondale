#include<stdio.h>
#include<cuda.h>
#include <time.h>
#include <sys/time.h>
#include<thrust/sort.h>
#include<stdlib.h>
#include<thrust/host_vector.h>
#include<thrust/device_vector.h>


long clock(struct timeval t, struct timeval s){
    return ((s.tv_sec * 1e6 +s.tv_usec ) -  (t.tv_sec * 1e6 +t.tv_usec ) );
}

int main(){
    struct timeval start, end;
    for ( int i= 1000; i<= 1e9; i*=2 ){
        for ( int k=0; k< 1; k++ ){
            thrust::host_vector<float> h;
        thrust::device_vector<float> d;
        

        for ( int j=0; j< i; j++){
            h.push_back(rand()%1000);


        }
        d=h;

        gettimeofday(&start, NULL);
        thrust::sort(d.begin(),d.end());
        cudaDeviceSynchronize();
        gettimeofday(&end, NULL);

        printf(" %d, %ld \n", i, clock(start,end));}
    }

    return 0;
}