//Simple example of Scatterv related to PA3
#include <math.h>
#include <mpi.h>
#include <time.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include "math.h"
#include "image_template.h"
#include <omp.h>
#include "canny_edge.h"
#include "canny_edge_full.h"

int main(int argc, char **argv)
{

	MPI_Init(&argc,&argv);

	if(argc!=3)
	{
		printf("\n srun --partition=compute --nodes=# --ntasks=# ./exec image <number of ghost rows>\n");
	}

	float  sigma =  atof(argv[2]); //sigma
	int  openmpthreads =  atoi(argv[3]); //openmp threads
	#if defined(_OPENMP)
		omp_set_num_threads(4);
	#endif

	float *fullimage,*chunk;
	int width, height;
	int commrank,commsize;
	struct timeval start,end;
	float elapsedtime;
	//get individual ranks
	MPI_Comm_size(MPI_COMM_WORLD,&commsize);
	MPI_Comm_rank(MPI_COMM_WORLD,&commrank);


	if(commrank==0) //only rank 0 reads image
	{	read_image_template<float>(argv[1],&fullimage,&width,&height);
	}

	//Parallel computations start
	gettimeofday(&start,NULL);

    	int a =  round(2.5*sigma - 0.5);

	MPI_Bcast(&width,1,MPI_INT,0,MPI_COMM_WORLD); //bcasting width to other procs
	MPI_Bcast(&height,1,MPI_INT,0,MPI_COMM_WORLD); //bcasting height to other procs

	//All processors allocate their respective chunks
	chunk=(float *)malloc(sizeof(float)*width*(height));

	//scatterv params
	int *sendcounts,*displs;

	//rank 0 sets sendcounts and displs by properly
	//considering chunk size (height/commsize) and 
	//number of ghost rows, a

	if(!commrank) {
		sendcounts = (int *)malloc(sizeof(int)*commsize);
		displs = (int *)malloc(sizeof(int)*commsize);

		displs[0] = 0;
		sendcounts[0] = ((height/commsize)+a)*width;

		for(int i=1;i<commsize-1;i++) {
			displs[i] = ((height/commsize)*i-a)*width;
			sendcounts[i] = ((height/commsize)+2*a)*width;
		}

		displs[commsize-1] = ((height/commsize)*(commsize-1)-a)*width;
		sendcounts[commsize-1] = ((height/commsize)+a)*width;
	}

	//Unlike a single scatter, processes that receive different 
	//amounts should invoke MPI_Scatterv 
	//This is because recvcount is different at these processes

	if(commrank==0)
		MPI_Scatterv(fullimage,sendcounts,displs,MPI_FLOAT,chunk,(height/commsize+a)*width,MPI_FLOAT,0,MPI_COMM_WORLD); 
	else if (commrank==commsize-1)
		MPI_Scatterv(fullimage,sendcounts,displs,MPI_FLOAT,chunk,(height/commsize+a)*width,MPI_FLOAT,0,MPI_COMM_WORLD); 
	else
		MPI_Scatterv(fullimage,sendcounts,displs,MPI_FLOAT,chunk,(height/commsize+2*a)*width,MPI_FLOAT,0,MPI_COMM_WORLD); 

	gettimeofday(&end,NULL);

	//if(commrank==0)	

	//Have processes write their chunks (image chunk+ghost rows)
	//For Debugging only
	char name[1000];
	sprintf(name,"chunk_%d.pgm",commrank);


	int myheight=(commrank==0||commrank==(commsize-1))?a:2*a;
	myheight+=height/commsize;
	//write_image_template<float>(name,chunk,width,myheight);


	float *  edge_linking = (float *) malloc( sizeof(float) * width * myheight );
	canny_edge_full(chunk, width, myheight, sigma, &edge_linking);


	char fname[1000];
	sprintf(fname,"finished_%d.pgm",commrank);
	//write_image_template<float>(fname,edge_linking,width,myheight);

	float * final_image = (float* ) malloc(sizeof(float) * width * height);

	edge_linking=(commrank==0)?edge_linking:(edge_linking+width*a); //slice off ghost row

	MPI_Gather(edge_linking, (height/commsize)*width, MPI_FLOAT, final_image, (height/commsize)*width, MPI_FLOAT, 0, MPI_COMM_WORLD);

	if(commrank==0) write_image_template<float>("final_image.pgm",final_image,width,height);





	//rank 0 is obviously the last to finish. Rank 0 calculates the time
	if(commrank==0) {

		elapsedtime = (end.tv_sec*1000 + end.tv_usec/1000) - (start.tv_sec*1000 + start.tv_usec/1000);

		// printf("\n Image Size, Sigma, MPI ranks, elapsed time (ms)");
		printf("\n %d, %d, %f",width,commsize,elapsedtime);
	}

	MPI_Finalize();
}
