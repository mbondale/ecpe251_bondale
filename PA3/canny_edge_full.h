#include "canny_edge.h"
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>

void canny_edge_full(float * imggray, int width, int height, float sigma, float ** edge_linking){

    
    float * kernel,  * kerneld;
    int kwidth, kwidthd;
    guassiankernel(&kernel, &kwidth, sigma);
     
    guassianderivativekernel(&kerneld, &kwidthd, sigma);
    
    kernelflipping(&kerneld, kwidthd);

    // horizontal
    float  * temphorizontal= (float*) malloc(sizeof(float)*width*height);
    
    int kw = 1;
    int kh = kwidth;
    //convolution_2(imggray, height,width, kernel, kh, kw, &temphorizontal);
    convolution(&imggray, &height,&width, &kernel, &kh, &kw, &temphorizontal);

    //for(int i=0; i< 100; i++) printf("%f, ", temphorizontal[i]);
       
    float  * horizontal= (float*) malloc(sizeof(float)*width*height);
    kw=kwidth;
    kh=1;
    convolution(&temphorizontal, &height,&width, &kerneld, &kh, &kw, &horizontal);

    
   // vertical
    float  * tempvertical= (float*) malloc(sizeof(float)*width*height);
    
     kw = kwidth;
     kh = 1;
    convolution(&imggray, &width,&height, &kernel, &kw, &kh , &tempvertical);
    free(imggray);
     
    float  * vertical= (float*) malloc(sizeof(float)*width*height);
    kw=1;
    kh=kwidth;
    convolution(&tempvertical, &width, &height, &kerneld, &kw, &kh , &vertical);
    
    //magnitude
    float  * magnitude= (float*) malloc(sizeof(float)*width*height);
    _magnitude(vertical, horizontal, &magnitude, width, height);
    
    //direction
    float  * direction= (float*) malloc(sizeof(float)*width*height);
    _direction(vertical, horizontal, &direction, width, height);

    //suppression
    float  * suppression= (float*) malloc(sizeof(float)*width*height);
    _suppression(direction, magnitude, &suppression, width, height);
    
    //edge_linking
    _edge_linking(suppression,edge_linking, width, height);


}


