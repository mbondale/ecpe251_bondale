#!/bin/bash
#SBATCH --partition=compute   ### Partition
#SBATCH --job-name=PA3 ### Job Name
#SBATCH --time=03:00:00     ### WallTime
#SBATCH --nodes=1         ### Number of Nodes
#SBATCH --tasks-per-node=4 ### Number of tasks (MPI processes=nodes*tasks-per-node. In this case, 4

for sig in 0.6 1.1 
do
	for((i=1024;i<=8192;i=i*2)) 
	do #each image 1024, 2048, and 4096, 8192
		for((j=0;j<5;j++)) 
		do	#each image executed 5 times
			srun ./canny_edge ~/LENNA_IMAGES/Lenna_org_$i.pgm $sig 4
		done
	done
	for((j=0;j<5;j++))
	do
		srun ./canny_edge ~/LENNA_IMAGES/Lenna_org_10240.pgm $sig 4
	done
done

#for((i=0;i<300;i++)) do #30 statistical runs
#srun ./canny_edge ~/LENNA_IMAGES/Lenna_org_1024.pgm 0.6
#done
