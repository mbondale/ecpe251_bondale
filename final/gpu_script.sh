#!/bin/bash

for size in 256 512 1024 2048 3072 
do
    for blocksize in 8 16 32
    do
        for((i=0;i<30;i++))
        do 
            ./adf_gpu ./lennas/Lenna_org_$size.pgm ./lennas/Lenna_noised_$size.pgm $blocksize
        done
    done
done