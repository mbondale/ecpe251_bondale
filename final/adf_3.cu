#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"image.h"
#include <sys/time.h>
#include "cuda.h"
#include <numeric>
#include <thrust/reduce.h>
#include<iostream>
#include "gpu_accumulate.h"


#define WINDOW 3
#define THRESH 40
#define STEP 0.25f
#define CONST 150.0f
#define ITERS 25
#define PASSES 1

#define ROWBLOCK 8
#define COLBLOCK 8

#include "adf_kernels.h"

int threads, blocks;
long long  clock(struct timeval s, struct timeval e){
	return (e.tv_sec*1000000+e.tv_usec)-(s.tv_sec*1000000+s.tv_usec);
}




float PSNR_gpu2( int *d_img1, int *d_img2, int width, int height){
	float psnr;
	float MSE =0.0f;
	float * error = (float *)malloc(sizeof(float)*width*height);
	float * d_error;
	cudaMalloc((void **) &d_error, sizeof(float)*height*width);

	PSNR_gpukernel<<<blocks, threads>>>(d_img1, d_img2, d_error, width, height);
	cudaDeviceSynchronize();

	cudaMemcpy(error, d_error, sizeof(int)*(height)*(width), cudaMemcpyDeviceToHost);

	float mse = std::accumulate(error, error+height*width, 0.0f );


	MSE=mse/(height*width);
	psnr= 10 * (float)log10(255.0*255.0/MSE);
	return(psnr);
}


void mediankernel_gpu2(int *d_ip_img, int *d_op_img, int height,int width){
	mediankernel_gpukernel<<< blocks, threads >>> (d_ip_img, d_op_img, height, width );
	cudaDeviceSynchronize();

}


void filterPDE_kernel_gpu2(int *d_ip_img, int *d_med_img, int *d_op_img,int height,int width){
	
	filterPDE_kernel_gpukernel<<< blocks, threads >>> (d_ip_img, d_med_img, d_op_img, height, width);
	cudaDeviceSynchronize();

}




/*

pseudocode:

read image -> org_img
add padding
 - org_img -> org_img

read image -> noised_img
add padding
 - noised_img -> noised_img

PSNR
mediankernel
filterPDE_kernel



*/

int main(int argc, char **argv)
{
	
	
//	test_accumulate(); return 0;



	//Variables required throughout the code
	int width,height;
	int *org_img, *noised_img, *op_img, *tmp_ip_img,*tmp_img ;
	float psnr;
	if(argc<3)
	{
		printf("No image file specified!\nExiting...\n");
		printf("\n do: ./adf <image> <noised image>\n");
		exit(0);
	}


	//Load Original Image
	read_image_template<int>(argv[1],&org_img,&width,&height);
	add_padding<int>(&org_img,width,height);
	
	
	int blocksize = atoi(argv[3]);
	threads = blocksize*blocksize;
	blocks = (width+2)*(height+2)/threads;

	//Load Noised Image
	read_image_template<int>(argv[2],&noised_img,&width,&height);
	add_padding<int>(&noised_img,width,height);
	
	//Output Image
	
	op_img = (int *)malloc(sizeof(int)*(width+2)*(height+2));
 

	tmp_ip_img = (int *)malloc(sizeof(int)*(width+2)*(height+2));
	tmp_img = (int *)malloc(sizeof(int)*(width+2)*(height+2));
 
	int *dorg_img, *dnoised_img, *dop_img, *dtmp_ip_img,*dtmp_img;

	cudaMalloc((void **) &dorg_img, sizeof(int)*(height+2)*(width+2));
	cudaMalloc((void **) &dnoised_img, sizeof(int)*(height+2)*(width+2));
	cudaMalloc((void **) &dop_img, sizeof(int)*(height+2)*(width+2));
	cudaMalloc((void **) &dtmp_ip_img, sizeof(int)*(height+2)*(width+2));
	cudaMalloc((void **) &dtmp_img, sizeof(int)*(height+2)*(width+2));


	cudaMemcpy(dorg_img, org_img, sizeof(int)*(height+2)*(width+2), cudaMemcpyHostToDevice);
	cudaMemcpy(dnoised_img, noised_img, sizeof(int)*(height+2)*(width+2), cudaMemcpyHostToDevice);
	struct timeval start,end, start_psnr, end_psnr, start_median, end_median, start_filter, end_filter, start_copy, end_copy;

	gettimeofday(&start,NULL);

	//Calculate PSNR

	gettimeofday(&start_psnr,NULL);

	float opsnr=PSNR_gpu2(dorg_img,dnoised_img, width,height);
	float fpsnr;
	gettimeofday(&end_psnr,NULL);

	for(int k=0; k<PASSES; k++)
	{
		gettimeofday(&start_copy,NULL);

		memcpy_gpu<<< blocks, threads >>>(dtmp_ip_img,dnoised_img,(width+2)*(height+2));
		cudaDeviceSynchronize();

		gettimeofday(&end_copy,NULL);

		for(int i=0;i<ITERS;i++)
		{

			//Median Kernel 
		
		//	mediankernel (tmp_ip_img, tmp_img,height,width);
			gettimeofday(&start_median,NULL);
			mediankernel_gpu2 (dtmp_ip_img, dtmp_img,height,width);
			gettimeofday(&end_median,NULL);

		//	filterPDE_kernel(tmp_ip_img,tmp_img,op_img,height,width);	
			gettimeofday(&start_filter,NULL);
			filterPDE_kernel_gpu2 (dtmp_ip_img,dtmp_img,dop_img,height,width);	
			gettimeofday(&end_filter,NULL);

			#if 1
			int *dtmp_ptr=dop_img;
			dop_img=dtmp_ip_img;
			dtmp_ip_img=dtmp_ptr;				
			#endif
		}
		

		 fpsnr=PSNR_gpu2(dorg_img,dop_img,width,height);

	}

	gettimeofday(&end,NULL);

	cudaMemcpy(op_img, dop_img, sizeof(int)*(height+2)*(width+2), cudaMemcpyDeviceToHost);



	printf("%d, %f, %f, %ld, %ld, %ld, %ld, %ld\n", height, opsnr, fpsnr, clock(start, end), clock(start_copy, end_copy), clock(start_psnr, end_psnr), clock(start_median, end_median), clock(start_filter, end_filter) );

	//Write the image
	remove_padding<int>(&op_img,width,height);
	char name[]="op.pgm";
	write_image_template<int>(name,op_img,width,height);
	
	//Clean up	
	free(org_img);
	free(noised_img);
	free(op_img);
	free(tmp_ip_img);
	free(tmp_img);
			
	return 0; 
}
