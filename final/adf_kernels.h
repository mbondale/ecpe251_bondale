


__global__ void memcpy_gpu(int * dest, int * source, int size){
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	if( i < size){
		dest[i]=source[i];
	}
}

__global__ void PSNR_gpukernel(int * img1, int * img2, float * error, int width, int height){
    int i = threadIdx.x + blockIdx.x*blockDim.x;
	if( i < width*height){
		int x = i/width, y = i%height;
		float err = (img1[(x+1)*(width+2)+(y+1)]-img2[(x+1)*(width+2)+(y+1)]);
		error[i]=err*err;


	}
}

__global__ void mediankernel_gpukernel(int *ip_img, int *op_img, int width, int height){
	int i = threadIdx.x + blockIdx.x*blockDim.x;
	if( i < width*height){

		int x,y,k,m;
		int center_value;
		int array[9],counter;
		counter=0;

		x=i/width;
		y=i%width;
		
		for(k=-1;k<=1;k++)
			for(m=-1;m<=1;m++)
					array[counter++]=ip_img[(x+k+1)*(width+2)+(y+m+1)];
		center_value=array[4];



		int s, e,f=0;
		int temp, c;
		int p;
		s=0;
		e=8;
		while (f != 1){
			p=array[e];
			i = s-1;
			for(c=s;c<=e;c++) {
				if(array[c] < p){
					i++;
					temp = array[c];
					array[c] = array[i];
					array[i]=temp;
				}

			}
			temp = array[e];
			array[e] = array[i+1];
			array[i+1]=temp;
			if(i+1 == 4){
				f=1;

			}
			else{
				if( i+1 > 4)
					e=i;

				if( i+1 < 4)
					s=i+2;

			}
		}
		
		int a4 = array[i+1];

		
        op_img[(x+1)*(width+2)+(y+1)]=(int)(abs(center_value-a4)>THRESH);
	}
}

__global__ void filterPDE_kernel_gpukernel(int *ip_img, int *med_img, int *op_img,int height,int width){
	int tx = threadIdx.x + blockIdx.x*blockDim.x;
	if( tx < width*height){
		int i=tx/width, j= tx%width;
		int pn,ps,pe,pw,pc;
		int mn,ms,me,mw;
		int gn,gs, ge, gw;
		float cn, cs, ce, cw;

		//Input Image
		pn=ip_img[(i-1+1)*(width+2)+(j+0+1)];
		ps=ip_img[(i+1+1)*(width+2)+(j+0+1)];
		pe=ip_img[(i+0+1)*(width+2)+(j-1+1)];
		pw=ip_img[(i+0+1)*(width+2)+(j+1+1)];
		pc=ip_img[(i+0+1)*(width+2)+(j+0+1)];

		//Median Pixels
		mn=med_img[(i-1+1)*(width+2)+(j+0+1)];
		ms=med_img[(i+1+1)*(width+2)+(j+0+1)];
		me=med_img[(i+0+1)*(width+2)+(j-1+1)];
		mw=med_img[(i+0+1)*(width+2)+(j+1+1)];
		//mc=med_img[(i+0+1)*(width+2)+(j+0+1)];

     	gn=(pn-pc)*(int)(mn==0.0);
     	gs=(ps-pc)*(int)(ms==0.0);
      	ge=(pe-pc)*(int)(me==0.0);
       	gw=(pw-pc)*(int)(mw==0.0);

       	cn=(CONST*CONST)/(CONST*CONST+gn*gn);
       	cs=(CONST*CONST)/(CONST*CONST+gs*gs);
       	ce=(CONST*CONST)/(CONST*CONST+ge*ge);
       	cw=(CONST*CONST)/(CONST*CONST+gw*gw);

		op_img[(i+1)*(width+2)+(j+1)]=pc+(int)(STEP*(cn*gn+cs*gs+ce*ge+cw*gw));
	}
}
