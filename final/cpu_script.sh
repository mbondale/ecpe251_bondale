#!/bin/bash

for size in 256 512 1024 2048 3072 
do
    for((i=0;i<30;i++))
        do 
            ./adf_cpu ./lennas/Lenna_org_$size.pgm ./lennas/Lenna_noised_$size.pgm
        done
done