
#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include "cuda.h"
#include <numeric>

__global__ void accumulate_gpu(float * array, int size, int stage){
	if (stage>30) return; // avoid INT overflow
	long  i = threadIdx.x + blockIdx.x*blockDim.x;
	long  x = size/1<<stage + 1;
	if( i < x){
	
		int a = i*(1<<stage);
		int b = i*(1<<stage) + (1<<(stage-1));

		if (a < size && b < size){
			array[a] = array[a]+array[b];
			array[b] = 0;
		}
		
	}
}

float accumulate_gpu_driver(float * array, int size){

	for ( int j=1; (1<<j)<size*2; j++){
		accumulate_gpu<<<15, 1000>>>(array, size, j);
		cudaDeviceSynchronize();
	}

	float * result;
	cudaMemcpy(result, array, sizeof(float)*1, cudaMemcpyDeviceToHost);
	return *result;
}



void test_accumulate(){
	int is=25630;
	float * array;
	array = (float *)malloc(sizeof(float)*(is));
	float  * darray;
	cudaMalloc((void **) &darray, sizeof(float)*(is));
	for ( int i=0; i<is;i++){
		array[i]= 0.2;
	}

	cudaMemcpy(darray, array, sizeof(float)*(is), cudaMemcpyHostToDevice);

	float result = accumulate_gpu_driver(darray, is);

	cudaMemcpy(array, darray , sizeof(float)*(is), cudaMemcpyDeviceToHost);

	printf("%f \n ", result);

	return;
}