Execute: ./canny_stage1 <path to image> <sigma value>

temp horizontal image is stored  <path to image>_h_t.pgm
horizontal is stored as <path to image>_h.pgm
temp vertical image is stored  <path to image>_v_t.pgm
vertical is stored as <path to image>_v.pgm

magnitude is stored  <path to image>_m.pgm
direction is stored  <path to image>_d.pgm
