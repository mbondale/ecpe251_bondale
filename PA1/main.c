#include <time.h>
#include <sys/time.h>
#include <omp.h>
#include <stdlib.h>
#include <stdio.h>
#include "image_template.h"

#define PI 3.14

void _suppression(float * direction, int * magnitude, int ** suppression, int width, int height){

    #if defined(_OPENMP)
     #pragma omp parallel for default(private)
     #endif
    for(int i =0 ; i< width*height; i++){
        int y = i%width; // column
        int x = i/width; // row 
        float angle = direction[i];
        if (angle < 0 ) angle += PI;
        angle = angle*180/ PI ;
        if(angle <= 22.5 && angle > 157.5){
            (*suppression)[i] = magnitude[i];
            if ( x>0 ) if ( magnitude[i] < magnitude[ (x-1)*width + y]) (*suppression)[i] = 0;
            if (x < (height -1 )) if (magnitude[i] < magnitude[(x+1)*width + y ]) (*suppression)[i]= 0;

        }
        if(angle > 22.5 && angle <= 67.5 ){
            (*suppression)[i] = magnitude[i];
            if ( x>0 && y>0 ) if ( magnitude[i] < magnitude[ (x-1)*width + y-1]) (*suppression)[i] = 0;
            if (x < (height -1) && y<(width-1) ) if (magnitude[i] < magnitude[(x+1)*width + y+1 ]) (*suppression)[i]= 0;
        }
        if(angle > 67.5 && angle <= 112.5){
             (*suppression)[i] = magnitude[i];
            if ( y>0 ) if ( magnitude[i] < magnitude[ (x)*width + y-1]) (*suppression)[i] = 0;
            if (y<width-1 ) if (magnitude[i] < magnitude[(x)*width + y+1 ]) (*suppression)[i]= 0;
        }
        if(angle > 112.5 && angle <= 157.5){
             (*suppression)[i] = magnitude[i];
            if ( x>0 && y<width-1 ) if ( magnitude[i] < magnitude[ (x-1)*width + y+1]) (*suppression)[i] = 0;
            if (x < height-1 && y > 0 ) if (magnitude[i] < magnitude[(x+1)*width + y-1 ]) (*suppression)[i]= 0;

        }
    }

}

int comp (const void * elem1, const void * elem2) 
{
    int f = *((int*)elem1);
    int s = *((int*)elem2);
    if (f > s) return  1;
    if (f < s) return -1;
    return 0;
}

void _edge_linking(int * suppression, int ** edge_linking, int width, int height){
  
    qsort (suppression, sizeof(suppression)/sizeof(*suppression), sizeof(*suppression), comp);
    int t_high = suppression[(int)((width*height)*0.9) - 1];
    int t_low = t_high/5;
    int  * hysteresis = (int * ) malloc(sizeof(int)*width*height);
    #if defined(_OPENMP)
     #pragma omp parallel for default(private)
     #endif
    for(int i  =0 ;i <width*height;i++){
        if(suppression[i] > t_high) hysteresis[i] = 255;
        else if(suppression[i ]< t_low ) hysteresis[i] = 0;
        else hysteresis[i] = 125;

    }
    #if defined(_OPENMP)
     #pragma omp parallel for default(private)
     #endif
    for(int i  =0 ;i <width*height;i++){
        if (hysteresis[i]==125){
        (*edge_linking)[i] = 0;

        int x = i/width;
        int y = i%width;

        if(x>0) if(hysteresis[(x-1)*width+y] ==255) (*edge_linking)[i] = 255;
        if(x>0 && y>0) if(hysteresis[(x-1)*width+y-1] ==255) (*edge_linking)[i] = 255;
        if(x>0 && y<width-1) if(hysteresis[(x-1)*width+y+1] ==255) (*edge_linking)[i] = 255;

        if(y>0) if(hysteresis[(x)*width+y-1] ==255) (*edge_linking)[i] = 255;
        if(y<width-1) if(hysteresis[(x)*width+y+1] ==255) (*edge_linking)[i] = 255;

        if(x<height-1) if(hysteresis[(x+1)*width+y] ==255) (*edge_linking)[i] = 255;
        if(x<height-1 && y>0) if(hysteresis[(x+1)*width+y-1] ==255) (*edge_linking)[i] = 255;
        if(x<height-1 && y<width-1) if(hysteresis[(x+1)*width+y+1] ==255) (*edge_linking)[i] = 255;
        
        }

    }



}


void _magnitude(int * vertical, int * horizontal, int ** magnitude, int width, int height){
     #if defined(_OPENMP)
     #pragma omp parallel for default(private)
     #endif
    for(int i = 0; i< width*height; i++){
        (*magnitude)[i] = sqrt(vertical[i]*vertical[i] + horizontal[i]*horizontal[i]);

    }
}
void _direction(int * vertical, int * horizontal, float ** direction, int width, int height){
    #if defined(_OPENMP)
     #pragma omp parallel for default(private)
     #endif
    for(int i = 0; i< width*height; i++){
        (*direction)[i] = atan2(horizontal[i], vertical[i]);

    }
}



void swap( float * a, float * b){
    float temp = *a;
    *a = *b;
    *b = temp;

}

void kernelflipping(float ** kernel, int width){
for(int i=0; i<(floor(width/2)); i++){
     swap(&(*kernel)[i] ,  &(*kernel)[width-1-i]);
}
}

void guassianderivativekernel(float ** kernel, int *width, float sigma){
    int a =  round(2.5*sigma - 0.5);
    *width = 2*a+1;
    float sum = 0 ;
       (*kernel) = (float* ) malloc(sizeof(float*)*(*width));
    for(int i=0; i<(*width); i++){
        (*kernel)[i]= -1*(i-a)*exp((-1*(i-a)*(i-a))/(2*sigma*sigma));
       
        sum = sum - i* (*kernel)[i];
   }
     for(int i=0; i<(*width); i++){
        (*kernel)[i] = (*kernel)[i]/sum;
     
    }
}

void guassiankernel(float ** kernel, int * width, float sigma){
    int a =  round(2.5*sigma - 0.5);
    *width = 2*a+1;
    float sum = 0 ;
    (*kernel) = (float* ) malloc(sizeof(float*)*(*width));
    #if defined(_OPENMP)
#pragma omp parallel for default(private) reduction(+:sum)
#endif
    for(int i=0; i<(*width); i++){
        (*kernel)[i]= exp((-1*(i-a)*(i-a))/(2*sigma*sigma));
      
        sum = sum + (*kernel)[i];
     
    }
    #if defined(_OPENMP)
#pragma omp parallel for default(private)
#endif
    for(int i=0; i<(*width); i++){
        (*kernel)[i] = (*kernel)[i]/sum;
       
    }
}


void convolution(int ** pimage, int * pwidth, int * pheight, float ** pkernel, int * pkwidth, int * pkheight, int ** poutput){
// getting parameters value
int * image = *pimage;
int width = *pwidth;
int height = *pheight;
float * kernel = *pkernel;
int kwidth = *pkwidth;
int kheight = *pkheight;

// temp variable
float temp = 0.0;

#if defined(_OPENMP)
#pragma omp parallel for default(private)
#endif
for ( int x = 0; x < width*height ; x++){
    float sum = 0;
    for( int y = 0; y < kwidth*kheight; y++){

        // offset of y in kernel matrix
        int offseti = -1*floor(kheight/2) + floor(y/kwidth);
        int offsetj = -1*floor(kwidth/2) + floor(y%kwidth);

        // adding offset to x in image matrix
        int y_pos = (offseti+(int)floor(x/width) )*width + (offsetj+(int)floor(x%width));

        // mulitply respective kernel cell and image cell and add to sum
        if ( y_pos < width*height 
            && y_pos  >= 0 )
                temp = (float)image[  y_pos  ]*kernel[y];

                sum = sum + temp;        
    }

    //
    (*poutput)[x] =  (int)sum;
}
}




int main(int argc, char **argv)
{

    #if defined(_OPENMP)
        omp_set_num_threads(omp_get_num_procs());
    #endif
  if (argc < 2)
    {
      printf("USAGE: %s <image-file> <sigma value> \n", argv[0]);
      return 1;
    }



  struct timeval start, end;

  gettimeofday(&start, NULL);

   float sigma;
   char * imgname;
   int height, width;
   if(argc==3){
     imgname =argv[1];
     sigma = atof(argv[2]);
   }
   else{
       printf("specify file and sigma");
       return 0;
   }

 
    

    float * kernel,  * kerneld;
    int kwidth, kwidthd;
    guassiankernel(&kernel, &kwidth, sigma);
     
    guassianderivativekernel(&kerneld, &kwidthd, sigma);
    
    kernelflipping(&kerneld, kwidthd);
    
 for(int i =0; i<kwidth; i++){
      printf("%f ", kernel[i]);
  }
  
  printf("\n");

   for(int i =0; i<kwidthd; i++){
      printf("%f ", kerneld[i]);
  }
printf("\n");
    int * imggray;
    read_image_template(imgname, &imggray, &width, &height);

    // horizontal
    int  * temphorizontal= (int*) malloc(sizeof(int)*width*height);
    int  * horizontal= (int*) malloc(sizeof(int)*width*height);
    int kw = 1;
    int kh = kwidth;
    convolution(&imggray, &width,&height, &kernel, &kw, &kh , &temphorizontal);

       char hnamet[256];
    strcpy(hnamet, imgname);
    strtok(hnamet, ".");
    strcat(hnamet, "_h_t.pgm");
    puts(hnamet);

    write_image_template(hnamet, temphorizontal, width, height);

        kw=kwidth;
    kh=1;
    convolution(&temphorizontal, &width, &height, &kerneld, &kw, &kh , &horizontal);
    free(temphorizontal);

    char hname[256];
    strcpy(hname, imgname);
    strtok(hname, ".");
    strcat(hname, "_h.pgm");
    puts(hname);

    write_image_template(hname, horizontal, width, height);
    
   // vertical
    int  * tempvertical= (int*) malloc(sizeof(int)*width*height);
    int  * vertical= (int*) malloc(sizeof(int)*width*height);
     kw = kwidth;
     kh = 1;
    convolution(&imggray, &width,&height, &kernel, &kw, &kh , &tempvertical);
    free(imggray);
     char vnamet[256];
    strcpy(vnamet, imgname);
    strtok(vnamet, ".");
    strcat(vnamet, "_v_t.pgm");
    puts(vnamet);
   write_image_template(vnamet, tempvertical, width, height);

    kw=1;
    kh=kwidth;
    convolution(&tempvertical, &width, &height, &kerneld, &kw, &kh , &vertical);
    free(tempvertical );

  
    char vname[256];
    strcpy(vname, imgname);
    strtok(vname, ".");
    strcat(vname, "_v.pgm");
    puts(vname);
   write_image_template(vname, vertical, width, height);

    //magnitude
int  * magnitude= (int*) malloc(sizeof(int)*width*height);
_magnitude(vertical, horizontal, &magnitude, width, height);
    char mfile[] ="";
    char mname[256];
    strcpy(mname, imgname);
    strtok(mname, ".");
    strcat(mname, "_m.pgm");
    puts(mname);
   write_image_template(mname, magnitude, width, height);
    
    //direction
float  * direction= (float*) malloc(sizeof(float)*width*height);
_direction(vertical, horizontal, &direction, width, height);

    char dname[256];
    strcpy(dname, imgname);
    strtok(dname, ".");
    strcat(dname, "_d.pgm");
    puts(dname);

    for(int i=0; i<100; i++){
        printf(" %f ", direction[i]);
    }

   write_image_template(dname, direction, width, height);

 int  * suppression= (int*) malloc(sizeof(int)*width*height);
   _suppression(direction, magnitude, &suppression, width, height);

    char sname[256];
    strcpy(sname, imgname);
    strtok(sname, ".");
    strcat(sname, "_s.pgm");
    puts(sname);

     write_image_template(sname, suppression, width, height);

     int  * edge_linking= (int*) malloc(sizeof(int)*width*height);
   _edge_linking(suppression,&edge_linking, width, height);

    char hystname[256];
    strcpy(hystname, imgname);
    strtok(hystname, ".");
    strcat(hystname, "_hyst.pgm");
    puts(hystname);

     write_image_template(hystname, edge_linking, width, height);
   
  gettimeofday(&end, NULL); 

  printf("%ld\n", ((end.tv_sec * 1000000 + end.tv_usec)
		  - (start.tv_sec * 1000000 + start.tv_usec)));

  return 0;
}