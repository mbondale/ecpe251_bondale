#include <omp.h>
#include<stdio.h>



int main(){
      #if defined(_OPENMP)
        int tid = omp_get_num_procs();
        omp_set_num_threads(tid);
        printf("%d ", tid);
    #endif
    
    return 0;

}