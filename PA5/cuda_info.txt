Device number: 0
  Device name: Quadro P4000
  Compute capability: 6.1

  Clock Rate: 1480000 kHz
  Total SMs: 14 
  Shared Memory Per SM: 98304 bytes
  Registers Per SM: 65536 32-bit
  Max threads per SM: 2048
  L2 Cache Size: 2097152 bytes
  Total Global Memory: 8513978368 bytes
  Memory Clock Rate: 3802000 kHz

  Max threads per block: 1024
  Max threads in X-dimension of block: 1024
  Max threads in Y-dimension of block: 1024
  Max threads in Z-dimension of block: 64

  Max blocks in X-dimension of grid: 2147483647
  Max blocks in Y-dimension of grid: 65535
  Max blocks in Z-dimension of grid: 65535

  Shared Memory Per Block: 49152 bytes
  Registers Per Block: 65536 32-bit
  Warp size: 32

Device number: 1
  Device name: Quadro P4000
  Compute capability: 6.1

  Clock Rate: 1480000 kHz
  Total SMs: 14 
  Shared Memory Per SM: 98304 bytes
  Registers Per SM: 65536 32-bit
  Max threads per SM: 2048
  L2 Cache Size: 2097152 bytes
  Total Global Memory: 8513978368 bytes
  Memory Clock Rate: 3802000 kHz

  Max threads per block: 1024
  Max threads in X-dimension of block: 1024
  Max threads in Y-dimension of block: 1024
  Max threads in Z-dimension of block: 64

  Max blocks in X-dimension of grid: 2147483647
  Max blocks in Y-dimension of grid: 65535
  Max blocks in Z-dimension of grid: 65535

  Shared Memory Per Block: 49152 bytes
  Registers Per Block: 65536 32-bit
  Warp size: 32

Device number: 2
  Device name: Quadro P4000
  Compute capability: 6.1

  Clock Rate: 1480000 kHz
  Total SMs: 14 
  Shared Memory Per SM: 98304 bytes
  Registers Per SM: 65536 32-bit
  Max threads per SM: 2048
  L2 Cache Size: 2097152 bytes
  Total Global Memory: 8513978368 bytes
  Memory Clock Rate: 3802000 kHz

  Max threads per block: 1024
  Max threads in X-dimension of block: 1024
  Max threads in Y-dimension of block: 1024
  Max threads in Z-dimension of block: 64

  Max blocks in X-dimension of grid: 2147483647
  Max blocks in Y-dimension of grid: 65535
  Max blocks in Z-dimension of grid: 65535

  Shared Memory Per Block: 49152 bytes
  Registers Per Block: 65536 32-bit
  Warp size: 32

Device number: 3
  Device name: Quadro P4000
  Compute capability: 6.1

  Clock Rate: 1480000 kHz
  Total SMs: 14 
  Shared Memory Per SM: 98304 bytes
  Registers Per SM: 65536 32-bit
  Max threads per SM: 2048
  L2 Cache Size: 2097152 bytes
  Total Global Memory: 8513978368 bytes
  Memory Clock Rate: 3802000 kHz

  Max threads per block: 1024
  Max threads in X-dimension of block: 1024
  Max threads in Y-dimension of block: 1024
  Max threads in Z-dimension of block: 64

  Max blocks in X-dimension of grid: 2147483647
  Max blocks in Y-dimension of grid: 65535
  Max blocks in Z-dimension of grid: 65535

  Shared Memory Per Block: 49152 bytes
  Registers Per Block: 65536 32-bit
  Warp size: 32

