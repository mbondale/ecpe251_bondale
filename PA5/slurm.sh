#!/bin/bash

for sigma in 0.6 
do
	for size in 3072 5120 7680
	do
			for blocksize in 8 16 32 
			do
					for((j=0;j<10;j++)) 
					do	
						./Canny_Edge ./../lenna/Lenna_org_$size.pgm $sigma $blocksize >> pa5_5.csv
					done
			done
	done
done

