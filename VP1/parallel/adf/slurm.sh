#!/bin/bash
#SBATCH --partition=compute   ### Partition
#SBATCH --job-name=PA3 ### Job Name
#SBATCH --time=03:00:00     ### WallTime
#SBATCH --nodes=1         ### Number of Nodes
#SBATCH --tasks-per-node=1 ### Number of tasks (MPI processes=nodes*tasks-per-node. In this case, 2

for threads in 4 8 16
do
	for size in 256 512 1024 2048 3072
	do 
		for((j=0;j<5;j++)) 
		do	
			srun ./adf ~/lennas/Lenna_org_$size.pgm ~/lennas/Lenna_noised_$size.pgm $threads
		done
	done
done

