Run 'make' and 'Canny_Edge <image> <sigma>'

canny_edge.h
image_template.h
	Libraries for functions used in serial_main.c

Makefile
	make file

serial.csv
	Data from cluster runs

serial_home.csv
	Data from PC runs

serial_main.c
	Serial code for canny edge detection algorithm
