#include <time.h>
#include <sys/time.h>

#include <stdlib.h>
#include <stdio.h>
#include "math.h"
#include "image_template.h"
#include "canny_edge.h"

long clock(struct timeval t, struct timeval s){
    return ((s.tv_sec * 1000000 + s.tv_usec) - (t.tv_sec * 1000000 + t.tv_usec));
}

int main(int argc, char **argv)
{

    
  if (argc<3)
    {
      printf("USAGE: %s <image-file> <sigma value> \n", argv[0]);
      return 1;
    }


  struct timeval start_end2end, end_end2end, start_comp, end_comp;
  struct timeval start_conv, end_conv, start_magdir, end_magdir, start_suppression, end_suppression, 
  start_sort, end_sort, start_hysteresis, end_hysteresis, start_edge, end_edge;
  
  gettimeofday(&start_end2end, NULL);

   float sigma;
   char * imgname;
   int height, width;

    imgname = argv[1];
    sigma   = atof(argv[2]);


   
   
    

    
    float * imggray;
    read_image_template(imgname, &imggray, &width, &height);

    gettimeofday(&start_comp, NULL);

    
    float * kernel,  * kerneld;
    int kwidth, kwidthd;
    guassiankernel(&kernel, &kwidth, sigma);
    guassianderivativekernel(&kerneld, &kwidthd, sigma);
    kernelflipping(&kerneld, kwidthd);

    gettimeofday(&start_conv, NULL);


    // horizontal
    float  * temphorizontal= (float*) malloc(sizeof(float)*width*height);
    
    int kw = 1;
    int kh = kwidth;
    convolution(imggray, height,width, kernel, kh, kw, temphorizontal);

       
    float  * horizontal= (float*) malloc(sizeof(float)*width*height);
    kw=kwidth;
    kh=1;
    convolution( temphorizontal,  height, width, kerneld,kh, kw , horizontal);

    
   // vertical
    float  * tempvertical= (float*) malloc(sizeof(float)*width*height);
    
     kw = kwidth;
     kh = 1;
    convolution(imggray, width, height, kernel, kw, kh , tempvertical);
    free(imggray);
     
    float  * vertical= (float*) malloc(sizeof(float)*width*height);
    kw=1;
    kh=kwidth;
    convolution(tempvertical,width, height, kerneld, kw, kh , vertical);
    gettimeofday(&end_conv, NULL);

    
    gettimeofday(&start_magdir, NULL);

    //magnitude
    float  * magnitude= (float*) malloc(sizeof(float)*width*height);
    _magnitude(vertical, horizontal, &magnitude, width, height);
    
    
    
    //direction
    float  * direction= (float*) malloc(sizeof(float)*width*height);
    _direction(vertical, horizontal, &direction, width, height);

  
    gettimeofday(&end_magdir, NULL);

        //suppression
    gettimeofday(&start_suppression, NULL);
    float  * suppression= (float*) malloc(sizeof(float)*width*height);
    _suppression(direction, magnitude, &suppression, width, height);
    gettimeofday(&end_suppression, NULL);


      //sort
    gettimeofday(&start_sort, NULL);
    float * sorted_suppression = (float*) malloc(sizeof(float)*width*height);
    for(int i=0;i<width*height;i++)sorted_suppression[i]=suppression[i];
    qsort (sorted_suppression, sizeof(sorted_suppression)/sizeof(*sorted_suppression), sizeof(*sorted_suppression), comp);
    gettimeofday(&end_sort, NULL);


      //hysteresis
    gettimeofday(&start_hysteresis, NULL);
    float * hysteresis = (float*) malloc(sizeof(float)*width*height);
    _hysteresis(suppression,sorted_suppression, &hysteresis, width, height);
    gettimeofday(&end_hysteresis, NULL);


       //edge_linking
    gettimeofday(&start_edge, NULL);
    float  * edge_linking= (float*) malloc(sizeof(float)*width*height);
    _edge_linking(hysteresis,&edge_linking, width, height);
    gettimeofday(&end_edge, NULL);

    gettimeofday(&end_comp, NULL);


    write_image_template("temphorizontal.pgm", temphorizontal, width, height);
    free(temphorizontal);

 
    write_image_template("tempvertical.pgm", tempvertical, width, height);
    free(tempvertical);

   
    write_image_template("horizontal.pgm", horizontal, width, height);
    free(horizontal);


    write_image_template("vertical.pgm", vertical, width, height);
    free(vertical);
    
    
    write_image_template("magnitude.pgm", magnitude, width, height);
    free(magnitude);


    write_image_template("direction.pgm", direction, width, height);
    free(direction);
    
 
    write_image_template("suppression.pgm", suppression, width, height);
    free(suppression);


    write_image_template("edge.pgm", edge_linking, width, height);
    free(edge_linking);
    
    gettimeofday(&end_end2end, NULL); 


    int size = width*height;

/*
struct timeval start_end2end, end_end2end, start_comp, end_comp;
  struct timeval start_conv, end_conv, start_magdir, end_magdir, start_suppression, end_suppression, 
  start_sort, end_sort, start_hysteresis, end_hysteresis, start_edge, end_edge;
  */
    printf("%d, ", width);
    printf("%f, ", sigma);
    printf("%ld, ", clock(start_comp, end_comp));
    printf("%ld, ", clock(start_end2end, end_end2end));
    printf("%ld, ", clock(start_conv, end_conv));
    printf("%ld, ", clock(start_magdir, end_magdir));
    printf("%ld, ", clock(start_suppression, end_suppression));
    printf("%ld, ", clock(start_sort, end_sort));
    printf("%ld, ", clock(start_hysteresis, end_hysteresis));
    printf("%ld ", clock(start_edge, end_edge));
    printf("\n");

  
  return 0;
}