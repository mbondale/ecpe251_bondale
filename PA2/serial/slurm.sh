#!/bin/bash

for sigma in 0.6 
do
	for size in 256 512 1024 2048 4096 8192 10240 12800 
	do
		for((j=0;j<3;j++)) 
		do	
						./Canny_Edge ./../../lenna/Lenna_org_$size.pgm $sigma  >> serial.csv
		done
			
	done
done

